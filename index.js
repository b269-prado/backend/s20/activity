// console.log("Hello World!");

/*
	1. Create a loop that will print out a number if it's divisible by 5 based on a number provided by the user. If the number is less than 50, the loop will stop. if the number is divisible by 10, the number will not be printed and will be skipped.

*/	

	let num = prompt("Enter a number:");
	console.log("The number you provided is: " +num);

	for (let i = num; i >= 0; i--) {
	  if (i % 5 === 0 && i % 10 !== 0 && i > 50 ) {
	    console.log(i);
	  }

	  if (i % 10 === 0 && i !== 50 && i > 50){
	    console.log("The number is divisible by 10. Skipping the number.");
	  }

	  if (i === 50){
	    console.log("The current value is 50. terminating the loop.");
	  }
	  
	}

/*
	2. Create another loop that will take a string and store all the consonants into another variable removing all vowels from it.
	Example from instructions: supercalifragilisticexpialidocious

*/ 

	let word = "supercalifragilisticexpialidocious";
	console.log("supercalifragilisticexpialidocious");
	let consonants = "";

	for(let i = 0; i < word.length; i++){	

		if(
			word[i].toLowerCase() == "a" ||
			word[i].toLowerCase() == "e" ||
			word[i].toLowerCase() == "i" ||
			word[i].toLowerCase() == "o" ||
			word[i].toLowerCase() == "u"     

			){
			
		}else{
			consonants += word[i];
		}

	}
	console.log(consonants); 

